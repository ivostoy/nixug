# nixug

Unix users and groups REST service

uses OpenAPI for API definition - see swagger.yml

uses goswagger tool to generate REST boilerplate code

see https://goswagger.io/


go get -u github.com/go-swagger/go-swagger/cmd/swagger

swagger init spec   --title "Unix users and groups query REST service"   --description "Braincorp homework"   --version 1.0.0   --scheme http   --produces application/json


swagger generate server -A nixug -f ./swagger.yml

To implement the core of your application you start by editing restapi/configure_todo_list.go. This file is safe to edit. Its content will not be overwritten if you run swagger generate again the future.


swagger validate ./swagger.yml


go get -u -f ./...

todo-list-server --help

rm -rf cmd models restapi

go run cmd/todo-list-server/main.go

go build cmd/todo-list-server/main.go

// ProducerFunc represents a function that can be used as a producer
type ProducerFunc func(io.Writer, interface{}) error

// Produce produces the response for the provided data
func (f ProducerFunc) Produce(writer io.Writer, data interface{}) error {
    return f(writer, data)
}

// Producer implementations know how to turn the provided interface into a valid
// HTTP response
type Producer interface {
    // Produce writes to the http response
    Produce(io.Writer, interface{}) error
}

Alice – Painless Middleware Chaining for Go

https://justinas.org/alice-painless-middleware-chaining-for-go

https://github.com/casualjim/middlewares

Gzip: gzips responses
Audit: go-metric capturing with yammer metrics compatible output
Healthchecks: go-metrics healthchecks with yammer metrics compatible output
Profiling: use pprof
Recovery: handle panics

