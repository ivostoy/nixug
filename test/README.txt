/etc/group file format:

name:
Password: not used, hence it is x/blank.
Group ID (GID): unique number
Group List: comma-delimited list users who are members of the group.

example:
docker:x:999:tap,ivos

response
[
{"name": "_analyticsusers", "gid": 250, "members": ["_analyticsd’,"_networkd","_timed"]}
]


The /etc/passwd file is a colon-separated file:
User name:
Password: not used, hence it is x/blank
User ID number (UID):
User's group ID number(GID):
Full name of the user:
User home directory:
Login shell

example:
root:x:0:0:root:/root:/bin/bash
systemd-timesync:x:100:102:systemd Time Synchronization,,,:/run/systemd:/bin/false


{"name": "dwoodlins", "uid": 1001, "gid": 1001, "comment": "", "home": "/home/dwoodlins", "shell": "/bin/false"}



https://github.com/fsnotify/fsnotify


watcher, err := fsnotify.NewWatcher()
if err != nil {
    log.Fatal(err)
}
defer watcher.Close()

done := make(chan bool)
go func() {
    for {
        select {
        case event, ok := <-watcher.Events:
            if !ok {
                return
            }
            log.Println("event:", event)
            if event.Op&fsnotify.Write == fsnotify.Write {
                log.Println("modified file:", event.Name)
            }
        case err, ok := <-watcher.Errors:
            if !ok {
                return
            }
            log.Println("error:", err)
        }
    }
}()

err = watcher.Add("/tmp/foo")
if err != nil {
    log.Fatal(err)
}
<-done