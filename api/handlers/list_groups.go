package handlers

import (
	"bitbucket.org/ivostoy/nixug"
	"bitbucket.org/ivostoy/nixug/gen/models"
	"bitbucket.org/ivostoy/nixug/gen/restapi/operations/group"
	//"log"

	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/swag"
)

func modelsError(err error) *models.Error {
	return &models.Error{
		Message: swag.String(err.Error()),
	}
}

// NewListGroups handles a request for list groups
func NewListGroups(rt *nixug.Runtime) group.ListHandler {
	return &listGroups{rt: rt}
}

type listGroups struct {
	rt *nixug.Runtime
}

// Handle the find known keys request
func (d *listGroups) Handle(params group.ListParams) middleware.Responder {
	var groups []*models.Group
	var g models.Group
	g.Name = "foo"
	g.Gid = 1
	groups = append(groups, &g)
	return group.NewListOK().WithPayload(groups)
}
