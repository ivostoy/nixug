PROJECT = nixug
export PORT=8080

SRCDIRS := ./...

#MOD=-mod=readonly
MOD=

PKGS := $(shell go list ./...)

STYLE1=monokai

HTTP=http --style=$(STYLE1)
AUTH=--auth-type=jwt --auth=$(shell http localhost:8484/v1/auth/tap/T@p)

#S := 1556218054463
S := 1556315241286
E :=
L := 10

ID1=users/1
ID2=users/2
ID3=groups/1
ID4=groups/2

GIT_REF = $(shell git rev-parse --short=8 --verify HEAD)
VERSION ?= $(GIT_REF)

export GO111MODULE=on

sw-install:
	go get -u github.com/go-swagger/go-swagger/cmd/swagger

sw-val:
	swagger validate ./swagger/swagger.yml

sw-gen:	
	swagger generate server -A $(PROJECT) -t gen -f ./swagger/swagger.yml
	#swagger generate server --exclude-main -A $(PROJECT) -t gen -f ./swagger/swagger.yml

sw-clean:
	@echo "*** danger, make sure no customization in restapi/operations"
	# rm -rf restapi/operations
	rm -rf cmd models restapi

build:
	@echo building nixug - output is in ./main
	go build cmd/nixug-server/main.go

run:
	go run cmd/nixug-server/main.go

help:
	@echo "sw-install - install goswagger (requires go)"
	@echo "sw-val - validate swagger.yml"
	@echo "sw-gen - generate server"
	@echo ---
	@echo "run    - runs app"
	@echo "build  - builds app"
	@echo ---

	@echo "h    - health check"
	@echo "getu  - get user"
	@echo "getg  - get group"
	@echo ---


test: install
	go test  ./...

testv: install
	go test -mod=readonly ./... -v

test-race: | test
	go test -race -mod=readonly ./...

vet: | test
	go vet ./...

fmt:
	go fmt ./...

check: test test-race vet gofmt

install:
	@#go install -mod=readonly -v -tags "" ./...
	go mod tidy
	go install -mod=readonly -v  ./...

download:
	go mod download


staticcheck:
	go install honnef.co/go/tools/cmd/staticcheck
	staticcheck \
		-checks all,-ST1003 \
		$(PKGS)

pedantic: check unparam errcheck

unparam:
	go install mvdan.cc/unparam
	unparam ./...

errcheck:
	go install github.com/kisielk/errcheck
	errcheck $(PKGS)

h:
	$(HTTP) $(HP)/health

getu:
	$(HTTP) $(META)/

